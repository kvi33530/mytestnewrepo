package utility;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class Base {

	public static ExtentHtmlReporter htmlReporter;
	public static ExtentReports extent;
	public static ExtentTest logger;
	public static WebDriver driver;

	
	@Parameters({"Browser","Url"})
	@BeforeMethod
		 public void LaunchBrowser(String browser,String Url ) {

		if (browser.equals("chrome")) {
			
			System.setProperty("webdriver.chrome.driver",
					"C:\\Users\\vitth\\OneDrive\\Desktop\\Techno\\chromedriver_win32\\chromedriver.exe");
			driver = new ChromeDriver();
			driver.get(Url);
			driver.manage().window().maximize();

		} else if (browser.equals("ie")) {

			System.setProperty("webdriver.ie.driver",
					"C:\\Users\\vitth\\OneDrive\\Desktop\\Techno\\IEDriverServer_x64_3.2.0\\IEDriverServer.exe");
			driver = new InternetExplorerDriver();
			driver.get(Url);
			driver.manage().window().maximize();

		}
	}

	@BeforeSuite
	public void startReport() {

		// IMP: use below 5 lines of code to save an extentreport as new copy everytime
		// based on timestamp
		/*
		 * Date date = new Date(); String Reportpath=
		 * "C:\\Users\\vitth\\OneDrive\\Desktop\\Techno\\Extentreports\\LatestReport";
		 * String fileextension=".html"; SimpleDateFormat formatter = new
		 * SimpleDateFormat("yyyyMMddhhmmss"); String filenewpath=
		 * Reportpath+formatter.format(date)+fileextension;
		 */

		htmlReporter = new ExtentHtmlReporter(
				"C:\\Users\\vitth\\OneDrive\\Desktop\\Techno\\Extentreports\\AIRVistaraExtent\\LatestReport.html");
		extent = new ExtentReports();
		extent.attachReporter(htmlReporter);
	}

	public void CloseBrowser() {
		driver.quit();
	}

	@AfterMethod
	public void getResult(ITestResult result) throws IOException {

		if (result.getStatus() == ITestResult.SUCCESS) {

			logger.log(Status.PASS, "Test Case Passed is " + result.getName());
			CloseBrowser();
		}

		if (result.getStatus() == ITestResult.FAILURE) {

			String screenShotPath = CommonActions.GetScreenshot(driver);
			logger.log(Status.FAIL, "Test Case Failed is " + result.getName() + result.getThrowable());
			// attach the screenshot to the report
			logger.fail("Snapshot below: " + logger.addScreenCaptureFromPath(screenShotPath));
			CloseBrowser();

		} else if (result.getStatus() == ITestResult.SKIP) {
			logger.log(Status.SKIP, "Test Case Skipped is " + result.getName());
			CloseBrowser();
		}

	}

	@AfterSuite
	public void endReport() {

		extent.flush();

	}

}
