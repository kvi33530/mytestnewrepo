package utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class CommonActions {


	public static String GetScreenshot(WebDriver driver) throws IOException {
		// type casting

		TakesScreenshot ts = (TakesScreenshot) driver;

		File source = ts.getScreenshotAs(OutputType.FILE);

		// IMP: use below 5 lines of code to capture screenshot after testcase failure
		// and save inside the new file
		Date date = new Date();
		String Screenshotpath = "D:\\SeleniumTraining\\image";
		String fileextension1 = ".png";
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddhhmmss");
		String dest = Screenshotpath + formatter.format(date) + fileextension1;

		// String dest = "D:\\SeleniumTraining\\image.png";
		File destination = new File(dest);

		FileUtils.copyFile(source, destination);

		return dest;
	}

	public Properties ReadPropertyFile() throws IOException {

		FileInputStream file = new FileInputStream(new File("AIExpress1.properties"));
		Properties pro = new Properties();
		pro.load(file);
		return pro;
	}


}
