package pages;

import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

public class TravelInformationNavigation {

	// Object repository

	@FindBy(xpath = "/html/body/div[1]/div/div/div/div/div/div/div/div/div/div/div/div[1]/div[1]/div[1]/div/div[1]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div[2]/button")
	WebElement Travel_information;

	@FindBy(xpath = "//*[@id=\"header\"]/div[2]/div[2]/div[1]/div/div/div[2]/div/div[2]/div/div[1]/div/div[4]/div[5]/a")
	WebElement GSTinformation;
	
	@FindBy(xpath="//*[@id=\"domain-modal\"]/div/div/form/div[1]/div/button/img")
	WebElement popup;
	
	@FindBy(xpath="//*[@id=\"header\"]/div[2]/div[2]/div[1]/div/div/div[2]/div")
	WebElement nav_bar;
	
	@FindBy(xpath="//*[@id=\"header\"]/div[2]/div[2]/div[1]/div/div/div[2]/div/div[2]/div/div[1]/div/div[4]/div[5]/a")
	WebElement GST;

	WebDriver driver;
	ExtentTest logger;

	public TravelInformationNavigation(WebDriver driver1, ExtentTest logger1) {
		this.driver = driver1;
		this.logger = logger1;
		PageFactory.initElements(driver, this);
	}
	
	public boolean NavigateTravelInfo() throws InterruptedException
	{
		logger.log(Status.INFO, "clicking on travel info");
		Thread.sleep(2000);
		logger.log(Status.INFO, "clicking on popup");
		popup.click();
		Thread.sleep(4000);
		//Travel_information.click();
		logger.log(Status.INFO, "mousehovering to travel info");
		Actions act=new Actions(driver);
		List<WebElement> lis= nav_bar.findElements(By.tagName("button"));
		for(WebElement val: lis)
		{
			if(val.getText().equals("Travel Information"))
			{
				val.click();
				act.moveToElement(val).build().perform();
				break;
			}
		}
		Thread.sleep(3000);
		String window1=driver.getWindowHandle();
		logger.log(Status.INFO, "clicking on GST information");
		GST.click();
		Set<String> allwindows=driver.getWindowHandles();
		logger.log(Status.INFO, "Navigating to GST information window");
		for(String val:allwindows )
		{
			if(!val.equalsIgnoreCase(window1))
			{
				driver.switchTo().window(val);
				break;
			}
		}
		boolean titlenamematch=false;
		logger.log(Status.INFO, "getting the title of the page");
		String title=driver.getTitle();
		System.out.println(title);
		if(title.equalsIgnoreCase("GST Web Portal"))
		{
			titlenamematch=true;
		}else
		{titlenamematch=false;}
		
		return titlenamematch;
		
	}
	

}
