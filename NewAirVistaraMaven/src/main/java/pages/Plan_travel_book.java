package pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

public class Plan_travel_book {
	// Object repository

	@FindBy(xpath = "/html/body/div[1]/div/div/div/div/div/div/div/div/div/div/div/div[1]/div[1]/div[1]/div/div[1]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div[1]/button")
	WebElement Plan_travel;
	
	@FindBy(xpath="//*[@id=\"header\"]/div[2]/div[2]/div[1]/div/div/div[2]/div")
	WebElement nav_bar;

	@FindBy(xpath = "//*[@id=\"header\"]/div[2]/div[2]/div[1]/div/div/div[2]/div/div[1]/div/div[1]/div/div[1]/div[2]/a")
	WebElement flights;
	
	@FindBy(id="domain-done")
	WebElement popup;

	WebDriver driver;
	ExtentTest logger;

	public Plan_travel_book(WebDriver driver1, ExtentTest logger1) {
		this.driver = driver1;
		this.logger = logger1;
		PageFactory.initElements(driver, this);
	}

	public boolean checkflightbookingNavigation() throws InterruptedException {
		boolean flag = false;

		Actions act = new Actions(driver);
		logger.log(Status.INFO, "closing the pop up widnow");
		Thread.sleep(3000);
		popup.click();
		Thread.sleep(3000);
		logger.log(Status.INFO, "Mousehovering on Plan travel");
	
		List<WebElement> lis= nav_bar.findElements(By.tagName("button"));
		for(WebElement val: lis)
		{
			if(val.getText().equals("Plan Travel"))
			{
				val.click();
				act.moveToElement(val).build().perform();
				
			}
			break;
		}
		
		logger.log(Status.INFO, "clicking on flights option");
		Thread.sleep(3000);
		flights.click();
		String title = driver.getTitle();
		logger.log(Status.INFO, "Expected"+"[Flights Tickets Booking Online | Lowest Air fare Tickets | Vistara)"+"Actual"+"["+title+"]");
		if (title.equals("Flights Tickets Booking Online | Lowest Air fare Tickets | Vistara")) {
			flag = true;
		} else {
			flag = false;
		}
		return flag;

	}

}
