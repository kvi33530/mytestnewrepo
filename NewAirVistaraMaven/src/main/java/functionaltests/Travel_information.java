package functionaltests;

import org.junit.Assert;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;


import pages.TravelInformationNavigation;
import utility.Base;

public class Travel_information extends Base {

	@Test
	public void Travel_info() throws InterruptedException
	{
		logger=extent.createTest("TC02: testcase to navigate to travel info page");
		logger.log(Status.INFO, "navigating to  travel info page");
		TravelInformationNavigation obj=new TravelInformationNavigation(driver,logger);
		boolean flag=obj.NavigateTravelInfo();
		Assert.assertTrue("user navigated to GST web portal screen", flag==true);
	}
	
	
	
}
