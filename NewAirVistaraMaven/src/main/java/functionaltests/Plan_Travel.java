package functionaltests;

import org.testng.annotations.Test;
import com.aventstack.extentreports.Status;

import junit.framework.Assert;
import pages.Plan_travel_book;
import utility.Base;

@SuppressWarnings("deprecation")
public class Plan_Travel extends Base{
	
	@SuppressWarnings("deprecation")
	@Test
	public void checkflights() throws InterruptedException
	{
		logger=extent.createTest("verify the user is able to navigate to book flights page");
		logger.log(Status.INFO, "navigating to the application");
		Plan_travel_book obj=new Plan_travel_book(driver, logger);
		boolean flag=obj.checkflightbookingNavigation();
		Assert.assertEquals(true, flag);
	
	}

}
